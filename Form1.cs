﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iEncryptor
{
    public partial class Form1 : Form
    {

        private static String fileName = "";
        private static String saveFileName = "";
        private static String data = "";

        public Form1()
        {
            InitializeComponent();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                FileDialog openDialog = new OpenFileDialog();
                openDialog.ShowDialog();
                fileName = openDialog.FileName;

                if (File.Exists(fileName))
                {
                    textBox2.Clear();
                    String line = "";
                    Console.WriteLine("Open file: " + fileName);
                    StreamReader stream = new StreamReader(fileName);

                    while ((line = stream.ReadLine()) != null)
                    {
                        textBox2.Text += EncryptAndDecrypt(line);
                    }
                    stream.Close();
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }            
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FileDialog saveDialog = new SaveFileDialog();
            saveDialog.ShowDialog();
            saveFileName = saveDialog.FileName;
            FileStream fs;
            try
            {
                fs = new FileStream(saveFileName, FileMode.Create); 
                StreamWriter writer = new StreamWriter(fs);
                writer.WriteLine(EncryptAndDecrypt(textBox2.Text));
                writer.Close();
                fs.Close();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public string EncryptAndDecrypt(string data)
        {
            int key = 128;
            //string cryptEnd = ":[Enctypted]" ;
            string str = "";

            if (encrypt.Text == "Encrypt")
            {
                if((data.LastIndexOf(":[Enctypted]")) != -1){
                    data = data.Remove(data.LastIndexOf(":[Enctypted]"));
                    encrypt.Text = "Decrypt";
                }
                
            }

            foreach(char ch in data)
            {
                str += (char)(ch ^ key);
            }
            return str;
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void encrypt_Click(object sender, EventArgs e)
        {
            if(encrypt.Text == "Encrypt"){
                textBox2.Text = EncryptAndDecrypt(textBox2.Text + ":[Enctypted]");
                encrypt.Text = "Decrypt";
            }
            else
            {
                textBox2.Text = EncryptAndDecrypt(textBox2.Text);
                encrypt.Text = "Encrypt";
            }
        }

        private void Password_CheckedChanged(object sender, EventArgs e)
        {
            if (!textBox1.Enabled)
            {
                //textBox1.Enabled = true;
            }
            else
            {
                //textBox1.Enabled = false;
            }
            MessageBox.Show("Not Relising, waiting 2.0 version");
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (textBox2.Text != "")
            {
                encrypt.Enabled = true;
            }
            else
            {
                encrypt.Enabled = false;
            }
        }
    }
}
